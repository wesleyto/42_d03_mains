#include <stdlib.h>
#include <stdio.h>
#include "ft_sort_integer_table.c"

void print_arr(int *nums, int size)
{
	int i = 0;
	while (i < size)
	{
		printf("%d ", nums[i]);
		i++;
	}
	printf("%s", "\n");
}

int		main(void)
{
	int nums[10] = {5, 3, 8, 7, 2, 6, 1, 9, 10, 4};
	print_arr(nums, 10);
	ft_sort_integer_table(nums, 10);
	print_arr(nums, 10);
	int nums2[0] = {};
	print_arr(nums2, 0);
	ft_sort_integer_table(nums2, 0);
	return (0);
}










