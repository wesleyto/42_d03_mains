#include <stdlib.h>
#include <stdio.h>
#include "ft_ultimate_ft.c"

int	main(void)
{
	int a = 3;
	int *b = &a;
	int **c = &b;
	int ***d = &c;
	int ****e = &d;
	int *****f = &e;
	int ******g = &f;
	int *******h = &g;
	int ********i = &h;
	int *********j = &i;
	ft_ultimate_ft(j);
	printf("42? %d. %s\n", a, a == 42 ? "Yes!": "No...");
}