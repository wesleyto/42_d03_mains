#include <stdlib.h>
#include <stdio.h>
#include "ft_atoi.c"

int	main(void)
{
	printf("atoi:\t%d\n",atoi("0"));
	printf("ft__:\t%d\n\n",ft_atoi("0"));
	printf("atoi:\t%d\n",atoi("1"));
	printf("ft__:\t%d\n\n",ft_atoi("1"));
	printf("atoi:\t%d\n",atoi("-1"));
	printf("ft__:\t%d\n\n",ft_atoi("-1"));
	printf("atoi:\t%d\n",atoi("123A123"));
	printf("ft__:\t%d\n\n",ft_atoi("123A123"));
	printf("atoi:\t%d\n",atoi("A123"));
	printf("ft__:\t%d\n\n",ft_atoi("A123"));
	printf("atoi:\t%d\n",atoi("-A123"));
	printf("ft__:\t%d\n\n",ft_atoi("-A123"));
	printf("atoi:\t%d\n",atoi("2147483647"));
	printf("ft__:\t%d\n\n",ft_atoi("2147483647"));
	printf("atoi:\t%d\n",atoi("2147483648"));
	printf("ft__:\t%d\n\n",ft_atoi("2147483648"));
	printf("atoi:\t%d\n",atoi("-2147483648"));
	printf("ft__:\t%d\n\n",ft_atoi("-2147483648"));
	printf("atoi:\t%d\n",atoi("-2147483649"));
	printf("ft__:\t%d\n\n",ft_atoi("-2147483649"));
	printf("atoi:\t%d\n",atoi("-3000000000"));
	printf("ft__:\t%d\n\n",ft_atoi("-3000000000"));
	printf("atoi:\t%d\n",atoi("-3000000000"));
	printf("ft__:\t%d\n\n",ft_atoi("-3000000000"));
	printf("atoi:\t%d\n\n",atoi(""));
	printf("ft__:\t%d\n\n",ft_atoi(""));
}
