#include <stdlib.h>
#include <stdio.h>
#include "ft_div_mod.c"

int	main(void)
{
	int a = 5;
	int b = 3;
	int div = 1000;
	int mod = 1000;
	ft_div_mod(a, b, &div, &mod);
	printf("Success? %s div=%d (exp. %d). mod=%d (exp. %d).\n", (div == a/b && mod == a%b) ? "Yes!" : "No...", div, a/b, mod, a%b);
	return (0);
}