#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "ft_putstr.c"

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

int		main(void)
{
	char *message = "Hello There! Success!";
	ft_putstr(message);
	char *message2 = "1";
	ft_putstr(message2);
	char *message3 = "";
	ft_putstr(message3);
	char *message4 = "Empty string above";
	ft_putstr(message4);
	return (0);
}