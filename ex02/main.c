#include <stdlib.h>
#include <stdio.h>
#include "ft_swap.c"

int	main(void)
{
	int a = 3;
	int b = 5;
	ft_swap(&a, &b);
	printf("Swapped? a=%d (was 3). b=%d (was 5). %s\n", a, b, a == 5 && b == 3 ? "Yes!": "No...");
}