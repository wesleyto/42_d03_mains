#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ft_strlen.c"


int		main(void)
{
	char *message = "This is a test string";
	printf("%s\n", ft_strlen(message) == (int)strlen(message) ? "S-U-C-C-E-E-S! That's the way you spell success!" : "Boo-Urns!");
	message = "";
	printf("%s\n", ft_strlen(message) == (int)strlen(message) ? "Success!" : "Failure");
	return (0);
}
