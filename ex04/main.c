#include <stdlib.h>
#include <stdio.h>
#include "ft_ultimate_div_mod.c"

int	main(void)
{
	int orig_a = 5;
	int orig_b = 3;
	int a = orig_a;
	int b = orig_b;
	ft_ultimate_div_mod(&a, &b);
	printf("Success? %s div=%d (exp. %d). mod=%d (exp. %d).\n", (a == orig_a/orig_b && b == orig_a%orig_b) ? "Yes!" : "No...", a, orig_a/orig_b, b, orig_a%orig_b);
	return (0);
}