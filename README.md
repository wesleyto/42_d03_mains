# README #

### What is this repository for? ###

* 42 d03 evaluations

### How do I get set up? ###

* Run norminette on the directory BEFORE adding these files
* Download (or clone) on the PC of the person to be corrected
* Once again, run norminette BEFORE adding these files
* Copy the files into the same directory (merge, if necessary)
* Run the compile script
* Execute each main to see the output

### Who do I talk to? ###

* Repo owner